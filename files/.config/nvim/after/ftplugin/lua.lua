-- Launch language server
require('lspconfig').lua_ls.setup({
	settings = {
		Lua = {
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = 'LuaJIT',
			},
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = { 'vim' },
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file('', true),

				-- Avoid using "Environment Emulation"
				-- @see https://github.com/sumneko/lua-language-server/wiki/Libraries#environment-emulation
				-- @see https://github.com/sumneko/lua-language-server/discussions/1688
				checkThirdParty = false,
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
			['color.mode'] = false,
		},
	},

	-- Setup completion engine
	capabilities = require('plugs.configs.autocmp').setup_for_source(),

	on_attach = function(client, bufnr)
		client.server_capabilities.semanticTokensProvider = nil
		require('keybindings.lsp').common(bufnr)
	end,
})

-- Call wrapper since lspstart occurs before sourcing ftplugin??
require('lspconfig').lua_ls.manager.try_add_wrapper()
