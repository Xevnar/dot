#!/usr/bin/sh

EXCLUDED_FILES='(\./init.lua|/lua/plugs/init\.lua|/lua/vlog/init\.lua|/lua/plugs/configs/treesitter\.lua|after/ftplugin/java\.lua|after/ftplugin/cs\.lua)'
stylua -v -f stylua.toml $(find . -iregex '.*\.lua' | grep -v -E "$EXCLUDED_FILES")
