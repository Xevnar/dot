local M = {}

function M.setup()
	return require('treesitter-context').setup({
		patterns = { -- Match patterns for TS nodes. These get wrapped to match at word boundaries.
			lua = {
				'variable_declaration',
				'elseif_statement',
			},
		},

		--exact_patterns = {
		--	-- Example for a specific filetype with Lua patterns
		--	-- Treat patterns.rust as a Lua pattern (i.e "^impl_item$" will
		--	-- exactly match "impl_item" only)
		--	-- rust = true,
		--},
	})
end

return M
